package paytm;

import java.util.LinkedList;
import java.util.List;

public class SMA implements ISMA {
	private List<Double> elements = new LinkedList<>();
	private int numValues = 1;
	private double sum = 0;
	private double sma = 0;
	
	public SMA (int numValues, double[] list) {
		if (numValues < 1) throw new Error("Period must be greater than 0");
		
		this.numValues = numValues;
		
		this.setSma(list);
	}

	/*
	 * SMA = lastSMA + lastElement/len(values) - firstElement/len(values)
	 */
	private void adjustElements() {
		while (this.elements.size() > this.numValues) {
			this.sum -= this.elements.remove(0);
		}
	}
	
	private void calculateSma() {
		this.sma = this.sum / this.numValues;
	}
	
	private void setSma(double item) {
		this.sum += item;
		this.elements.add(item);
		
		this.adjustElements();
		this.calculateSma();
	}
	
	private void setSma(double[] list) {
		// Start the list where the period covers the values
		int i = (list.length >= this.numValues) ? list.length - this.numValues : 0; 
		if (list.length >= this.numValues) {
			this.elements.clear();
			this.sum = 0;
		}
		
		for (; i < list.length; i++) {
			this.elements.add(list[i]);
			this.sum += list[i];
		}
		
		this.adjustElements();
		this.calculateSma();
	}
	
	@Override
	public double getSMA() {
		return this.sma;
	}

	@Override
	public double getSMA(double item) {
		this.setSma(item);
		return this.sma;
	}

	@Override
	public double getSMA(double[] list) {
		this.setSma(list);
		return this.sma;
	}
}
