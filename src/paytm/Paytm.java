package paytm;

import java.util.Arrays;

public class Paytm {
	static int numValues = 1;
	static double[] testData;
	
	public static void main(String[] args) {
		System.out.println(Arrays.toString(args));
		parseArgs(args);
		
		SMA smaThreeCounter = new SMA(numValues, testData);
		System.out.println("SMA: " + smaThreeCounter.getSMA());
	}
	
	public static void parseArgs(String[] args) {
		for (String arg : args) {
			if (arg.contains("numValues")) {
				System.out.println("numValues= " + arg.split("=")[1]);
				numValues = Integer.parseInt(arg.split("=")[1]);
			}
			
			if (arg.contains("values")) {
				System.out.println("values= " + arg.split("=")[1]);
				String[] values = arg.split("=")[1].split(",");
				testData = new double[values.length];
				
				for (int i = 0; i < values.length; i++) {
					testData[i] = Double.parseDouble(values[i]);
				}
				
				System.out.println(Arrays.toString(testData));
			}
		}
	}
}
