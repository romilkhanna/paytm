package paytm;

/*
 * Write an interface for a data structure that can provide the moving average of the last N elements added, 
 * add elements to the structure and get access to the elements. 
 * Provide an efficient implementation of the interface for the data structure.
 * Provide a separate interface (IE interface/trait) with documentation for the data structure
 * Provide an implementation for the interface
 * Provide any additional explanation about the interface and implementation in a README file.
 */

public interface ISMA {	
	public double getSMA();
	public double getSMA(double item);
	public double getSMA(double[] list);
}
