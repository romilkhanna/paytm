# PayTM Code Challenge

Write an interface for a data structure that can provide the moving average of the last N elements added, add elements to the structure and get access to the elements. Provide an efficient implementation of the interface for the data structure.

## Minimum Requirements
* Provide a separate interface (IE interface/trait) with documentation for the data structure
* Provide an implementation for the interface
* Provide any additional explanation about the interface and implementation in a README file.

## Usage
* Build project using any method (./bin/paytm), then:
  * paytm --numValues=<int> --values=<double[]>
    * Note: order of args does not matter

Test: { 1, 3, 5, 6, 8 }
Result: 6.333333333333333

## Intentional Omissions
* In the interest of time, values provided are assumed to be valid

# PayTM Design Question

Design A Google Analytic like Backend System. We need to provide Google Analytic like services to our customers. Please provide a high level solution design for the backend system. Feel free to choose any open source tools as you want.

## Requirements
* Handle large write volume: Billions of write events per day.
* Handle large read/query volume: Millions of merchants wish to gain insight into their business. Read/Query patterns are time-series related metrics.
* Provide metrics to customers with at most one hour delay.
* Run with minimum downtime.
* Have the ability to reprocess historical data in case of bugs in the processing logic.

## Solution summary

GA.pdf in root directory has architecture diagram of proposed design. Design assumes reasonable budget with general IOPS of >5k req/s. Design assumes AWS but relevant pieces can be swapped for equivalent in-house solutions.

Note: Proposed design is in current production environment supporting ~1B events daily (~5B max capacity) supporting 3 aggregation levels (minute, hour, day), >20 metrics with retention levels (36 hours, 1.5 quarters, 2 years) respectively.

### Synopsis

* Metric producing servers, push entries to their respective firehose.
* Firehose flushes data to ES cluster in respective index
  * Firehose flushes based on 50 MB buffer or 1 minute, whichever comes first
  * Firehose saves backup copy of metrics in S3 bucket as ground truth
    * S3 bucket has relevant retention policy
  * ES indices setup to rollover hourly
* Metric specific lambda executes aggregation query against ES cluster and writes data to second aggregated ES cluster
  * lambda executes based on cloudwatch timer every 5 minutes
  * lambda contains aggregation query for its metric
  * aggregated ES cluster is a different cluster
* Index specific lambda executes to cleanup old indices from their respective ES clusters
  * lambda executes based on cloudwatch timer based on retention policy
* As a backup for potentional backfill purposes or fallback in case of system failure, AWS Athena can be used to query raw logs from S3 buckets

### Examples

```
Log entry: { time: Date.now(), merchant: 1, price: 1 }
Metric: SMA
Firehose: fh-sma-prod
ES index: sma
```

or

```
Log entry: { time: Date.now(), merchant: 1, price: 1 }
Metric: sum
Firehose: fh-sum-prod
ES index: sum
```

...
